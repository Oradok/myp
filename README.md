# Myp

Simple web server based on nginx.

Return Client IP in plain text

## Build

    podman build -t myp ./

## Run 

    podman run --rm -p 80:8080 localhost/myp 

    # Persistent and detached

    podman run -p 8080:80 -d --name myp localhost/myp

## Test

    $ curl localhost:8080
    10.0.2.100%
  
## Extra

There is an extra header with the same result, just for fun ;)


